import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[]args){
		
		Scanner scan = new Scanner(System.in);
		
		Fridge[]appliance = new Fridge[4];
		
		for(int i = 0; i<appliance.length; i++){
			appliance[i] = new Fridge();
			System.out.println("What brand?");
			appliance[i].brand = scan.next();
			System.out.println("How big do you want the Fridge? (in inch)");
			appliance[i].size = scan.nextInt();
			System.out.println("You want a water dispenser with it?");
			appliance[i].water = scan.next();
		}
		
		System.out.println("Last Fridge stats: Brand: "+appliance[3].brand+" . Size: "+appliance[3].size+" . Water?: "+appliance[3].water);
		
		System.out.println("First Fridge Stats: ");
		appliance[0].whatBrand();
		appliance[0].whatSize();
	}
}